//
//  PlanTableViewCell.swift
//  TestApplication
//
//  Created by PRAHASITH AKKIRAJU on 9/11/16.
//  Copyright © 2016 Manthena,Suprem Raju. All rights reserved.
//

import UIKit

class PlanTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var postAuthor: UILabel!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var authorImageView: UIImageView!
    var logoImage: [String] = [
        "Chicago_willisTower",
        "Chicago_millenniumPark",
       "Chicago_360Tilt",
        "Chicago_FieldMuseum"
        
        
    ]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        authorImageView.layer.cornerRadius = authorImageView.frame.width / 2
        authorImageView.layer.masksToBounds = true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

    
    

