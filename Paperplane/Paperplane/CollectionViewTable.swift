//
//  CollectionViewTable.swift
//  SideBarAnimated
//
//  Created by PRAHASITH AKKIRAJU on 10/10/16.
//  Copyright © 2016 Prahasith Akkiraju. All rights reserved.
//

import Foundation
import UIKit


class CollectionViewTable : BaseCell {
    
    override var highlighted: Bool {
        didSet {
            backgroundColor = highlighted ? UIColor.darkGrayColor() : UIColor.whiteColor()
            nameLabel.textColor = highlighted ? UIColor.whiteColor() : UIColor.blackColor()
        }
    }
    
    var setting: Side? {
        didSet {
            nameLabel.text = setting?.name
        }
    }
    let nameLabel: UILabel = {
        let label = UILabel()
        //label.text = "Setting"
        //label.font = UIFont.systemFontOfSize()
        return label
    }()
    
    override func setupViews() {
        super.setupViews()
        addSubview(nameLabel)
        addConstraintsWithFormat("H:|-10-[v0]|", views:nameLabel)
        addConstraintsWithFormat("V:|[v0]|", views: nameLabel)
       
    
    }
}