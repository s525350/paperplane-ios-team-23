//
//  PlanningViewController.swift
//  Paperplane
//
//  Created by Rayaprolu,Naga Adithya on 10/20/16.
//  Copyright © 2016 Abbu,Srilatha Reddy. All rights reserved.
//

import UIKit
import MapKit

class PlanningViewController: UIViewController, UIGestureRecognizerDelegate {

    
    

    @IBOutlet weak var mapView: MKMapView!
    
    
    
    @IBOutlet weak var textView: UITextView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.mapTapped(_:)))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.mapTapped(_:)))
        //tap.delegate = self
        tap1.delegate = self
        tap2.delegate = self
        MainView.userInteractionEnabled = true
        //textView.userInteractionEnabled = true
        mapView.userInteractionEnabled = true
        //textView.addGestureRecognizer(tap)
        //MainView.addGestureRecognizer(tap1)
        mapView.addGestureRecognizer(tap2)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var MainView: UIView!
    func tapped() {
        print("In tapped")
        
        var frame1:CGRect = self.mapView.frame
        frame1.size.height = self.MainView.bounds.size.height * 0.7
        self.mapView.frame = frame1;
        var frame2:CGRect = self.textView.frame
        //frame2.size.height = self.textView.bounds.size.height * (self.view.frame.height-self.mapView.frame.height)
        frame2 = CGRectMake(10 , 200, self.MainView.frame.width, self.MainView.frame.height)
        self.textView.frame = frame2;
        
    }
    func mapTapped(recognizer:UITapGestureRecognizer) {
        //self.mapV.vc = self
        //        print("1")
        if self.mapView.frame.height != self.MainView.bounds.size.height * 0.7 {
            print("In maptapped")
            var frame:CGRect = self.MainView.frame
            frame.size.height = self.view.bounds.size.height * 0.85
            self.MainView.frame = frame;
            self.tapped()
        }
            
            //            var frame:CGRect = self.MainView.frame
            //            frame.size.height = self.view.bounds.size.height
            //            self.MainView.frame = frame;
            //            self.tapped()
            
            //print("2")
            //self.viewWillAppear(true)
        else {
            
            var frame:CGRect = self.MainView.frame
            //frame2.size.height = self.textView.bounds.size.height * (self.view.frame.height-self.mapView.frame.height)
            frame = CGRectMake( 0, 55, 600, 165)
            self.MainView.frame = frame;
            //self.returnView()
        }
    }
//    @IBAction func Clickbtn(sender: AnyObject) {
//        if self.mapView.frame.height != self.MainView.bounds.size.height * 0.7 {
//            print("In tapped")
//            var frame:CGRect = self.MainView.frame
//            frame.size.height = self.view.bounds.size.height
//            self.MainView.frame = frame;
//            self.tapped()
//        }
//        else {
//            
//            var frame:CGRect = self.MainView.frame
//            //frame2.size.height = self.textView.bounds.size.height * (self.view.frame.height-self.mapView.frame.height)
//            frame = CGRectMake( 0, 15, 600, 165)
//            self.MainView.frame = frame;
//            self.returnView()
//        }
//
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
