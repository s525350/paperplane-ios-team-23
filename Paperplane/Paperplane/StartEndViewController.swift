//
//  StartEndViewController.swift
//  SettingsPaperplane
//
//  Created by Akkiraju,Prahasith on 9/15/16.
//  Copyright © 2016 PRAHASITH AKKIRAJU. All rights reserved.
//

import UIKit

class StartEndViewController: UIViewController {
    var setting : settingsData!
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        setting = appDelegate.setting
        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.8)
        
        self.showAnimate()
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var doneBtn: UIButton!
    
    @IBOutlet weak var picker: UIDatePicker!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneBtn(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        //code for the conversion of the time to 24 to 12
        if(setting.timeFormat == "24 Hour") {
        dateFormatter.dateFormat = "hh:mm a"
        } else {
            dateFormatter.dateFormat = "HH:MM"
        }
        let strDate = dateFormatter.stringFromDate(picker.date)
       // print(strDate)
        if(setting.strend == false){
         setting.starttime = strDate
        } else {
            setting.endtime = strDate
        }
        
        self.removeAnimate()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
