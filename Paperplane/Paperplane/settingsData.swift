//
//  settingsData.swift
//  SettingsPaperplane
//
//  Created by PRAHASITH AKKIRAJU on 9/13/16.
//  Copyright © 2016 PRAHASITH AKKIRAJU. All rights reserved.
//

import Foundation

class settingsData {
    var home : HomeViewController!
    var timeFormat : String = "12 Hour"
    var selectedcurrency : String = "INR"
    var starttime : String = "10:10 AM"
    var endtime : String = "10:10 AM"
    var strend : Bool = false
    var curArray : [String] = [
        "USD - US Dollar",
        "EUR - Euro",
        "GBP - British Pound",
        "INR - Indian Rupee"
        ,"AUD - Australian Dollar"
        ,"SGD - Singapore Dollar"
        ,"CHF - Swiss Franc"
        ,"MYR - Malaysian Ringgit"
        ,"JPY - Japanese Yen"
        ,"CNY - Chinese Yuan Renminbi"
        ,"NZD - New Zealand Dollar"
        ,"THB - Thai Baht"
        ,"HUF - Hungarian Forint"
        ,"AED - Emirati Dirham"
        ,"HKD - Hong Kong Dollar"
        ,"MXN - Mexican Peso"
        ,"ZAR - South African Rand"
        ,"PHP - Philippine Peso"
        ,"SEK - Swedish Krona"
        ,"IDR - Indonesian Rupiah"
        ,"SAR - Saudi Arabian Riyal"
        ,"BRL - Brazilian Real"
        ,"TRY - Turkish Lira"
        ,"KES - Kenyan Shilling"
        ,"KRW - South Korean Won"
        ,"EGP - Egyptian Pound"
        ,"IQD - Iraqi Dinar"
        ,"NOK - Norwegian Krone"
        ,"KWD - Kuwaiti Dinar"
        ,"RUB - Russian Ruble"
        ,"DKK - Danish Krone"
        ,"PKR - Pakistani Rupee"
        ,"ILS - Israeli Shekel"
        ,"PLN - Polish Zloty"
        ,"QAR - Qatari Riyal"
        ,"XAU - Gold Ounce"
        ,"OMR - Omani Rial"
        ,"COP - Colombian Peso"
        ,"CLP - Chilean Peso"
        ,"TWD - Taiwan New Dollar"
        ,"ARS - Argentine Peso"
        ,"CZK - Czech Koruna"
        ,"VND - Vietnamese Dong"
        ,"MAD - Moroccan Dirham"
        ,"JOD - Jordanian Dinar"
        ,"BHD - Bahraini Dinar"
        ,"XOF - CFA Franc"
        ,"LKR - Sri Lankan Rupee"
        ,"UAH - Ukrainian Hryvnia"
        ,"NGN - Nigerian Naira"
        ,"TND - Tunisian Dinar"
        ,"UGX - Ugandan Shilling"
        ,"RON - Romanian New Leu"
        ,"BDT - Bangladeshi Taka"
        ,"PEN - Peruvian Sol"
        ,"GEL - Georgian Lari"
        ,"XAF - Central African CFA Franc BEAC"
        ,"FJD - Fijian Dollar"
        ,"VEF - Venezuelan Bolivar"
        ,"BYR - Belarusian Ruble"
        ,"HRK - Croatian Kuna"
        ,"UZS - Uzbekistani Som"
        ,"BGN - Bulgarian Lev"
        ,"DZD - Algerian Dinar"
        ,"DOP - Dominican Peso"
        ,"ISK - Icelandic Krona"
        ,"XAG - Silver Ounce"
        ,"CRC - Costa Rican Colon"]
}