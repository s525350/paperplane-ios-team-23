//
//  ViewController.swift
//  Preference
//
//  Created by SrilathaReddy on 11/15/16.
//  Copyright © 2016 Abbu, SrilathaReddy. All rights reserved.
//

import UIKit

class PreferenceViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    
    @IBOutlet weak var pickerView1: UIPickerView!
    
    @IBOutlet weak var pickerView2: UIPickerView!
    
    @IBOutlet weak var pickerView3: UIPickerView!
    
    @IBOutlet weak var pickerView4: UIPickerView!
    
    @IBOutlet weak var pickerView5: UIPickerView!
    
    var array = ["1", "2", "3", "4", "5"]
    var array1 = ["1", "2", "3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView1.delegate = self
        pickerView2.delegate = self
        pickerView3.delegate = self
        pickerView4.delegate = self
        pickerView5.delegate = self
        
        pickerView1.dataSource = self
        pickerView2.dataSource = self
        pickerView3.dataSource = self
        pickerView4.dataSource = self
        pickerView5.dataSource = self
    }

        
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(pickerView:UIPickerView, numberOfRowsInComponent: Int) -> Int {
        if pickerView == pickerView1 || pickerView == pickerView2 || pickerView == pickerView3 || pickerView == pickerView4 {
            return array.count
        } else if pickerView == pickerView5 {
            return array1.count
        }
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 || pickerView.tag == 1 || pickerView.tag == 2 || pickerView.tag == 3 {
            return array[row]
        } else if pickerView.tag == 4 {
            return array1[row]
        }
        return ""
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

