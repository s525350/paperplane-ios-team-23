//
//  RatingViewController.swift
//  SideBarAnimated
//
//  Created by PRAHASITH AKKIRAJU on 10/19/16.
//  Copyright © 2016 Prahasith Akkiraju. All rights reserved.
//

import UIKit

class RatingViewController: UIViewController {
    var settingLauncher : SidebarMain!
    override func viewDidLoad() {
        super.viewDidLoad()
        settingLauncher = SidebarMain()
        // Do any additional setup after loading the view.
    }

    @IBAction func OpenBtn(sender: AnyObject) {
     self.settingLauncher.showSide()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
