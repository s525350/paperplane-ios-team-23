//
//  CurrencyViewController.swift
//  SettingsPaperplane
//
//  Created by PRAHASITH AKKIRAJU on 9/14/16.
//  Copyright © 2016 PRAHASITH AKKIRAJU. All rights reserved.
//

import UIKit

class CurrencyViewController: UIViewController, UITableViewDataSource , UITableViewDelegate {
    var setting : settingsData!
    var currencies : [String]!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.8)
//        self.showAnimate()
        
        // Do any additional setup after loading the view
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    @IBAction func back(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //we have to make this app delegate evry where because it causes the problem pop up as viedidload  is called but not the every time...???
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        setting = appDelegate.setting
        let currencies  = setting.curArray
        return currencies.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            //we have to make this app delegate evry where because it causes the problem pop up as viedidload  is called but not the every time...???
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            setting = appDelegate.setting
            let currencies  = setting.curArray
            let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell")!
            cell.textLabel?.text = currencies[indexPath.row]
            return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("selection made in section\(indexPath.section) on row\(indexPath.row)")
        //we have to make this app delegate evry where because it causes the problem pop up as viedidload  is called but not the every time...???
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        setting = appDelegate.setting
        let currencies  = setting.curArray
        setting.selectedcurrency = currencies[indexPath.row]
        tableView.reloadData()
    }
    
    
    
        func showAnimate()
        {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            UIView.animateWithDuration(0.25, animations: {
                self.view.alpha = 1.0
                self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
            });
        }
        
        func removeAnimate()
        {
            UIView.animateWithDuration(0.25, animations: {
                self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
                self.view.alpha = 0.0;
                }, completion:{(finished : Bool)  in
                    if (finished)
                    {
                        self.view.removeFromSuperview()
                    }
            });
        }
        
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
         // Get the new view controller using segue.destinationViewController.
         // Pass the selected object to the new view controller.
         }
         */
        

}
