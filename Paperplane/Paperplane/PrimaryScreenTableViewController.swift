//
//  PrimaryScreenTableViewController.swift
//  TestApplication
//
//  Created by PRAHASITH AKKIRAJU on 9/11/16.
//  Copyright © 2016 Manthena,Suprem Raju. All rights reserved.
//

import UIKit

class PrimaryScreenTableViewController: UITableViewController {
    var settingLauncher : SidebarMain!
    @IBOutlet weak var listview: UIBarButtonItem!
    
    var locationImage: [UIImage] = [UIImage(named:"Seoul_Southkorea_1")!,
                                    UIImage(named:"Taipei_Taiwan_2")!,
                                    UIImage(named:"London_UK_3")!,
                                    UIImage(named:"Osaka_Japan_1")!,
                                    UIImage(named:"Newyork_USA_1")!]
    
    var locationName: [String] = ["Seoul, South Korea","Taipei, Taiwan","London, UK","Osaka, Japan","Newyork, USA"]
    
    
    @IBAction func listviewaction(sender: AnyObject) {
        self.settingLauncher.showSide()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        settingLauncher = SidebarMain()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return locationImage.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PrimaryScreenCell

        // Configure the cell...
        
            cell.ImageView.image = locationImage[indexPath.row]
            cell.locationTitle.text =  locationName[indexPath.row]
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "next") {
            let destination = segue.destinationViewController as! ViewController
            let row = tableView.indexPathForSelectedRow?.row
            destination.newImage.image = locationImage[row!]
        }
        }
        /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
