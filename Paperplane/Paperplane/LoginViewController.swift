//
//  LoginViewController.swift
//  Paperplane
//
//  Created by Abbu,Srilatha Reddy on 9/8/16.
//  Copyright © 2016 Abbu,Srilatha Reddy. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    let loginButton: FBSDKLoginButton = {
        let button = FBSDKLoginButton()
        button.readPermissions = ["email"]
        return button
    }()
    
    
    
    
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBAction func LoginBTN(sender: AnyObject) {
    
        if emailTF.text!.isEmpty && passwordTF.text!.isEmpty {
            displayAlertMessage ("All fields are required")
            return
        }
        
        if emailTF.text!.isEmpty {
            displayAlertMessage("Please enter a valid email")
            return
        }
        
        let validEmailID:Bool = isValidEmail(emailTF.text!)
        if validEmailID == false {
            displayAlertMessage("Please enter a valid email \n")
        }
        
        if passwordTF.text!.isEmpty {
            displayAlertMessage("Please enter your password")
            return
        }

        let validPassword: Bool = isValidPassword(passwordTF.text!)
        if validPassword == false {
            displayAlertMessage("Please enter a valid password \n")
        }
        
        let userEmail = emailTF.text
        let userPassword = passwordTF.text
        
        let emailStored = NSUserDefaults.standardUserDefaults().stringForKey("emailID")
        let passwordStored = NSUserDefaults.standardUserDefaults().stringForKey("password")
        
        if emailStored == userEmail {
            if passwordStored == userPassword {
                //login successful
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isUserLoggedIn")
                NSUserDefaults.standardUserDefaults().synchronize()
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }
    
    //email validation
    func isValidEmail(username:String) -> Bool {
        let emailRegex = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return emailTest.evaluateWithObject(username)
        
    }
    
    //password validation
    func isValidPassword(password:String) -> Bool {
        let passwordRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{6,15}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluateWithObject(password)
        
    }

    func fetchProfile () {
        print("fetch profile")
        
        let parameters = ["fields": "email, first_name, last_name, picture.type(large)"]
        FBSDKGraphRequest(graphPath: "me", parameters: parameters).startWithCompletionHandler { (connection, result, error) -> Void in
            if error != nil {
                print("error")
                return
            }
            if let email = result["email"] as? String {
                print(email)
            }
        }
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        print("completed login")
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
    func onError(message: String) {
        print("error\(message)")
        displayAlertMessage("Error!")
    }
    
    func onSuccess(message: String) {
        print("Successfully Logged in")
        displayAlertMessage("Successfully Logged in")
    }
    
    func displayAlertMessage(userMessage:String) {
        let alert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    func assignbackground(){
        let background = UIImage(named: "landing_2")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.ScaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignbackground()
        view.addSubview(loginButton)
        loginButton.center = CGPointMake(200.0, 150.0)
        loginButton.delegate = self
        
        if FBSDKAccessToken.currentAccessToken() != nil {
            fetchProfile()
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
