//
//  ScheduleViewController.swift
//  Paperplane
//
//  Created by Rayaprolu,Naga Adithya on 10/20/16.
//  Copyright © 2016 Abbu,Srilatha Reddy. All rights reserved.
//

import UIKit

class ScheduleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var j:Int = 6
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return j
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else  if section == 1 {
            return 1
        }
        else  if section == 2 {
            return 1
        }
        else  if section == 3 {
            return 1
        }
        else  if section == 4 {
            return 1
        }
        else {
            return 1
        }
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Day 1"
        }
        else  if section == 1 {
            return "Day 2"
        }
        else  if section == 2 {
            return "Day 3"
        }
        else  if section == 3 {
            return "Day 4"
        }
        else  if section == 4 {
            return "Day 5"
        }
        else {
            return "Day 6"
        }
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            //tableView.reloadData()
            let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath)
            let imageLBL = cell.viewWithTag(2) as! UIImageView
            imageLBL.image = UIImage(named: "new-zealand-wallpaper-5")
            let numberLBL:UILabel = cell.viewWithTag(3) as! UILabel
            numberLBL.text = "Places"
            //let imageLBL:UIImage = UIImage(imageLiteral: "")
            
            //let nameLBL:UILabel = cell.viewWithTag(2) as! UILabel
            //let numberLBL:UILabel = cell.viewWithTag(3) as! UILabel
            //nameLBL.text = "Hello"
            //numberLBL.text = "Hi"
            
            return cell
        }
        else if indexPath.section == 1 {
            //tableView.reloadData()
            let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath)
            let imageLBL = cell.viewWithTag(2) as! UIImageView
            imageLBL.image = UIImage(named: "new-zealand-wallpaper-5")
            let numberLBL:UILabel = cell.viewWithTag(3) as! UILabel
            numberLBL.text = "Places"
            //let imageLBL:UIImage = UIImage(imageLiteral: "")
            //            let nameLBL:UILabel = cell.viewWithTag(2) as! UILabel
            //            let numberLBL:UILabel = cell.viewWithTag(3) as! UILabel
            //            nameLBL.text = "Hello"
            //            numberLBL.text = "Hi"
            return cell
        }
        else if indexPath.section == 2 {
            //tableView.reloadData()
            let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath)
            let imageLBL = cell.viewWithTag(2) as! UIImageView
            imageLBL.image = UIImage(named: "new-zealand-wallpaper-5")
            let numberLBL:UILabel = cell.viewWithTag(3) as! UILabel
            numberLBL.text = "Places"
            //let imageLBL:UIImage = UIImage(imageLiteral: "")
            //            let nameLBL:UILabel = cell.viewWithTag(2) as! UILabel
            //            let numberLBL:UILabel = cell.viewWithTag(3) as! UILabel
            //            nameLBL.text = "Hello"
            //            numberLBL.text = "Hi"
            return cell
        }
        else if indexPath.section == 3 {
            //tableView.reloadData()
            let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath)
            let imageLBL = cell.viewWithTag(2) as! UIImageView
            imageLBL.image = UIImage(named:"new-zealand-wallpaper-5")
            let numberLBL:UILabel = cell.viewWithTag(3) as! UILabel
            numberLBL.text = "Places"
            //let imageLBL:UIImage = UIImage(imageLiteral: "")
            //            let nameLBL:UILabel = cell.viewWithTag(2) as! UILabel
            //            let numberLBL:UILabel = cell.viewWithTag(3) as! UILabel
            //            nameLBL.text = "Hello"
            //            numberLBL.text = "Hi"
            return cell
        }
        else if indexPath.section == 4 {
            //tableView.reloadData()
            let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath)
            let imageLBL = cell.viewWithTag(2) as! UIImageView
            imageLBL.image = UIImage(named:"new-zealand-wallpaper-5")
            let numberLBL:UILabel = cell.viewWithTag(3) as! UILabel
            numberLBL.text = "Places"
            //let imageLBL:UIImage = UIImage(imageLiteral: "")
            //            let nameLBL:UILabel = cell.viewWithTag(2) as! UILabel
            //            let numberLBL:UILabel = cell.viewWithTag(3) as! UILabel
            //            nameLBL.text = "Hello"
            //            numberLBL.text = "Hi"
            return cell
        }
        else {
            //tableView.reloadData()
            let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath)
            let imageLBL = cell.viewWithTag(2) as! UIImageView
            imageLBL.image = UIImage(named:"new-zealand-wallpaper-5")
            let numberLBL:UILabel = cell.viewWithTag(3) as! UILabel
            numberLBL.text = "Places"
            //let imageLBL:UIImage = UIImage(imageLiteral: "")
            //            let nameLBL:UILabel = cell.viewWithTag(2) as! UILabel
            //            let numberLBL:UILabel = cell.viewWithTag(3) as! UILabel
            //            nameLBL.text = "Hello"
            //            numberLBL.text = "Hi"
            return cell
        }
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //CODE TO BE RUN ON CELL TOUCH
        if indexPath.section == 1 {
            
        }
        
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let destination = segue.destinationViewController as? DeyailViewController
        let row = tableView.indexPathForSelectedRow?.row
        destination?.imageData = "new-zealand-wallpaper-5"

    }
    

}
