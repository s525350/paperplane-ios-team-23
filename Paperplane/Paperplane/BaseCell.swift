//
//  BaseCell.swift
//  SideBarAnimated
//
//  Created by PRAHASITH AKKIRAJU on 10/10/16.
//  Copyright © 2016 Prahasith Akkiraju. All rights reserved.
//
import UIKit

class BaseCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}