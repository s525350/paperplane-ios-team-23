//
//  ViewController.swift
//  TestApplication
//
//  Created by Gaddam,Snigdha on 9/5/16.
//  Copyright © 2016 Gaddam,Snigdha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    var locationView: PrimaryScreenTableViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        deptDateText.delegate = self
        desiredLocationImage.image = newImage.image
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    var newImage = UIImageView()
    
    @IBOutlet weak var desiredLocationImage: UIImageView!
    
    @IBOutlet weak var deptDateText: UITextField!
    
    @IBOutlet weak var retDateText: UITextField!
    
    @IBOutlet weak var arrivalTimeText: UITextField!
    
    @IBOutlet weak var departTimeText: UITextField!
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        closeKeyboard()
    }
    
    func closeKeyboard() {
        self.view.endEditing(true)
    }
    
    
    @IBAction func textFieldDidBeginEditing(sender: UITextField) {
        
        let deptDatePicker = UIDatePicker()
        
        deptDatePicker.datePickerMode = UIDatePickerMode.Date
        
        let retDatePicker = UIDatePicker()
        retDatePicker.datePickerMode = UIDatePickerMode.Date
        
        let arrivalTimePicker = UIDatePicker()
        arrivalTimePicker.datePickerMode = UIDatePickerMode.Time
        
        let departTimePicker = UIDatePicker()
        departTimePicker.datePickerMode = UIDatePickerMode.Time
        
        deptDateText.inputView = deptDatePicker
        
        retDateText.inputView = retDatePicker
        
        arrivalTimeText.inputView = arrivalTimePicker
        
        departTimeText.inputView = departTimePicker
        
        deptDatePicker.addTarget(self, action: #selector(ViewController.deptDatePickerChanged(_:)), forControlEvents: .ValueChanged)
        
        retDatePicker.addTarget(self, action: #selector(ViewController.retDatePickerChanged(_:)), forControlEvents: .ValueChanged)
        
        arrivalTimePicker.addTarget(self, action: #selector(ViewController.arrivalTimeChanged(_:)), forControlEvents: .ValueChanged)
        
        departTimePicker.addTarget(self, action: #selector(ViewController.departTimeChanged(_:)), forControlEvents: .ValueChanged)
        
    }
    
    
    
    func deptDatePickerChanged(sender: UIDatePicker) {
        
        
                let dateFormatter = NSDateFormatter()
               dateFormatter.dateFormat = "E, dd MMM"
        var date = NSDate()
        print("In changer")
        var isGreater = true
        
        //Compare Values
        if date.compare(sender.date) == NSComparisonResult.OrderedDescending {
            
            isGreater = false
            print(isGreater)
        }
        if isGreater == false {
            let alert = UIAlertController(title: "Date", message: "Please select a correct departure date", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)

        }
        else {
    let strDate = dateFormatter.stringFromDate(sender.date)
    self.deptDateText.text = strDate
                    }
    

        
    }
    
    func retDatePickerChanged(sender: UIDatePicker) {
        
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "E, dd MMM"
//
//        let rtnDate = dateFormatter.stringFromDate(sender.date)
//        self.retDateText.text = rtnDate
        var isGreater = true
        var date = NSDate()
        //Compare Values
        if date.compare(sender.date) == NSComparisonResult.OrderedDescending {
            
            isGreater = false
            print(isGreater)
        }
        if isGreater == false {
            let alert = UIAlertController(title: "Date", message: "Please select a correct return date", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        else {
            let strDate = dateFormatter.stringFromDate(sender.date)
            self.retDateText.text = strDate
        }
        
        
        
    }
    
    func arrivalTimeChanged(sender: UIDatePicker) {
        
        
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "HH:mm a"
        timeFormatter.timeZone = NSTimeZone(name: "CT")
        let arrivalTime = timeFormatter.stringFromDate(sender.date)
        self.arrivalTimeText.text = arrivalTime
        
        
        
    }
    
    func departTimeChanged(sender: UIDatePicker) {
        
        
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "HH:mm a"
        timeFormatter.timeZone = NSTimeZone(name: "CT")
        let departTime = timeFormatter.stringFromDate(sender.date)
        self.departTimeText.text = departTime
        
        
        
    }

}
