//
//  ViewController.swift
//  Paperplane
//
//  Created by Abbu,Srilatha Reddy on 9/8/16.
//  Copyright © 2016 Abbu,Srilatha Reddy. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    let loginButton: FBSDKLoginButton = {
       let button = FBSDKLoginButton()
        button.readPermissions = ["email"]
        return button
    }()
    
    
    @IBOutlet weak var emailIDTF: UITextField!

    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var firstNameTF: UITextField!
    
    @IBOutlet weak var lastNameTF: UITextField!
    
    @IBAction func createBTN(sender: AnyObject) {
        if emailIDTF.text!.isEmpty && passwordTF.text!.isEmpty && firstNameTF.text!.isEmpty && lastNameTF.text!.isEmpty {
            //display alert message
            displayAlertMessage ("All fields are required")
            return
        }
        if emailIDTF.text!.isEmpty {
            displayAlertMessage("Please enter a valid email")
            return
        }
        
        let validEmailID:Bool = isValidEmail(emailIDTF.text!)
        if validEmailID == false {
            displayAlertMessage("Please enter a valid email \n")
        }
        
        if passwordTF.text!.isEmpty {
            displayAlertMessage("Please enter your password")
            return
        }
        
        let validPassword: Bool = isValidPassword(passwordTF.text!)
        if validPassword == false {
            displayAlertMessage("Please enter a valid password \n")
        }
        
        if firstNameTF.text!.isEmpty {
            displayAlertMessage("Please do not leave your first name empty enter or use numbers and punctuations")
            return
        }
        if lastNameTF.text!.isEmpty {
            displayAlertMessage("Please do not leave your last name empty enter or use numbers and punctuations")
            return
        }
        
        //store Data
        NSUserDefaults.standardUserDefaults().setObject(emailIDTF.text, forKey: "emailID")
        NSUserDefaults.standardUserDefaults().setObject(passwordTF.text, forKey: "password")
         NSUserDefaults.standardUserDefaults().synchronize()
        
        
        //display alert message with confirmation
        let alert = UIAlertController(title: "Alert", message: "Successfully created your account!", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { action in
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        alert.addAction(okAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    func fetchProfile () {
        print("fetch profile")
        
        let parameters = ["fields": "email, first_name, last_name, picture.type(large)"]
        FBSDKGraphRequest(graphPath: "me", parameters: parameters).startWithCompletionHandler { (connection, result, error) -> Void in
            if error != nil {
                print("error")
                return
            }
            if let email = result["email"] as? String {
                print(email)
            }
        }
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        print("completed login")
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }

    
    //email validation
    func isValidEmail(username:String) -> Bool {
        let emailRegex = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return emailTest.evaluateWithObject(username)
        
    }
    
    //password validation
    func isValidPassword(password:String) -> Bool {
        let passwordRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{6,15}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluateWithObject(password)
        
    }
    
    func displayAlertMessage(userMessage:String) {
        let alert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    func assignbackground(){
        let background = UIImage(named: "landing_2")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.ScaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignbackground()
        view.addSubview(loginButton)
        loginButton.center = CGPointMake(200.0, 150.0)
       // loginButton.center = view.center
        loginButton.delegate = self
        
        if FBSDKAccessToken.currentAccessToken() != nil {
            fetchProfile()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

