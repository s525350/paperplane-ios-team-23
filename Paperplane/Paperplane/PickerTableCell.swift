//
//  PickerTableCell.swift
//  settings
//
//  Created by PRAHASITH AKKIRAJU on 10/24/16.
//  Copyright © 2016 Prahasith Akkiraju. All rights reserved.
//

import UIKit


class PickerTableCell : UITableViewCell {
     var frameAdded = false
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var labeltitle: UILabel!
   
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var selectedItem: UILabel!
    class var expandedHeight : CGFloat {get {return 200}}
    class var defaultHeight : CGFloat {get {return 44 }}
    func checkHeight() {
        datepicker.hidden = (frame.size.height < PickerTableCell.expandedHeight)
    }
    
    
    
    func watchFrameChanges(){
        if(!frameAdded){
        addObserver(self, forKeyPath: "frame", options: .New, context: nil)
        checkHeight()
        }
    }
    
    
    
    func ignoreFrameChanges(){
        if(frameAdded){
        removeObserver(self, forKeyPath: "frame")
            frameAdded = false
        }
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if keyPath == "frame" {
            checkHeight()
            
        }
    }
    
    
    deinit{
        print("deinit called")
        ignoreFrameChanges()
    }
}