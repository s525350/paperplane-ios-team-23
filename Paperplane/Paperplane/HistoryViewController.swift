//
//  HistoryViewController.swift
//  Paperplane
//
//  Created by PRAHASITH AKKIRAJU on 10/2/16.
//  Copyright © 2016 Abbu,Srilatha Reddy. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController , UITableViewDelegate,UITableViewDataSource  {

    var tableView: UITableView  =   UITableView()
    var data: HistoryCell!
    var rowData:Int!
    var settingLauncher : SidebarMain!

    @IBAction func listtable(sender: AnyObject) {
    self.settingLauncher.showSide()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        settingLauncher = SidebarMain()
        self.title="History"
        data = HistoryCell()
        //tableView = UITableView(frame: UIScreen.mainScreen().bounds, style: UITableViewStyle.Plain)
        tableView.delegate      =   self
        tableView.dataSource    =   self
        //tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        //self.view.addSubview(self.tableView)
        
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! HistoryCell
        
        if(indexPath.row == 0){
        cell.placeNew.text = "Itinerary to Sydney, AUS"
        cell.dateNew.text = "5/14/16 to 7/18/16"
            rowData = 0
        }
        else if( indexPath.row == 1){
            cell.placeNew.text = "Itinerary to Chicago, USA"
            cell.dateNew.text = "5/14/15 to 7/18/15"
            rowData = 1
        }
        else {
            cell.placeNew.text = "Itinerary to Capetown, SA"
            cell.dateNew.text = "5/14/14 to 7/18/14"
            rowData = 2
        }
        return cell;
    }
    
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        let destination = segue.destinationViewController as? HistoryDetailTableViewController
//        //let row = tableView.indexPathForSelectedRow!.row
//        destination?.imageData = data.newImage[rowData]
//        destination?.nowTitle = data.xtitle[rowData]
//        destination?.nextTitle = data.Day[rowData]
//    }
    
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


