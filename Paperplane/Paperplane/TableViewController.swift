//
//  TableViewController.swift
//  settings
//
//  Created by PRAHASITH AKKIRAJU on 10/24/16.
//  Copyright © 2016 Prahasith Akkiraju. All rights reserved.
//

import UIKit
let cellID = "cell"

class TableViewController: UITableViewController {
    var time : [String]!
    var link : [String]!
    var setting : settingsData!
    
    
    @IBAction func listbtn(sender: AnyObject) {
        self.settingLauncher.showSide()
    }
    
    var settingLauncher : SidebarMain!
    var selectedIndexPath : NSIndexPath?
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        settingLauncher = SidebarMain()
        time  = ["Defaul Start Time","Default End Time"]
         link  = ["Facebook", "Twitter", "Instagram"]
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        setting = appDelegate.setting
        print(setting.selectedcurrency)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 5
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1{
            return 1
        }else if section == 2{
            return 2
        }else if section == 3{
            return 1
        }else {
            return 3
        }
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellID, forIndexPath: indexPath) as! PickerTableCell
       
        if indexPath.section == 0 {
            cell.labeltitle.text = "Account"
            cell.selectedItem.text = ""
            cell.img.image = UIImage(named: "Account")
            return cell
        } else if indexPath.section == 1 {
            cell.labeltitle.text = "Currency"
            cell.selectedItem.text = setting.selectedcurrency
            cell.img.image = UIImage(named: "Currency")
            return cell
        } else if indexPath.section == 2 {
            cell.labeltitle.text = self.time[indexPath.row]
            if(time[indexPath.row] == "Defaul Start Time") {
                cell.img.image = UIImage(named: "Start")
                cell.selectedItem.text = setting.starttime
            } else {
                cell.img.image = UIImage(named: "End")
                cell.selectedItem.text = setting.endtime
            }
            return cell
        } else if indexPath.section == 3 {
            cell.labeltitle.text = "Time Format"
            cell.selectedItem.text = setting.timeFormat
            cell.img.image = UIImage(named: "add_green")
            return cell
        } else {
            cell.labeltitle.text = link[indexPath.row]
            cell.selectedItem.text = ""
            cell.img.image = UIImage(named: "\(link[indexPath.row])")
            return cell
        }

    }
 
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 2 {
        let previousIndexPath = selectedIndexPath
        if indexPath == selectedIndexPath{
            selectedIndexPath = nil
        } else {
            selectedIndexPath = indexPath
        }
        
        var indexPaths : Array<NSIndexPath> = []
        if let previous = previousIndexPath{
            indexPaths += [previous]
        }
        if let current = selectedIndexPath {
            indexPaths += [current]
        }
        if indexPaths.count > 0 {
            tableView.reloadRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.Automatic)
        }
        } else if indexPath.section  == 1 {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let vc : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("currency") as UIViewController
            self.presentViewController(vc, animated: true, completion: nil)
        } else if indexPath.section == 4 {
            if indexPath.row == 0 {
                UIApplication.sharedApplication().openURL(NSURL(string: "https://www.facebook.com/Paperplane-Travel-Planning-444195452407142/?fref=ts")!)
            } else if indexPath.row == 1 {
                UIApplication.sharedApplication().openURL(NSURL(string: "https://www.instagram.com/paperplane.travel/?hl=en")!)
            } else {
                UIApplication.sharedApplication().openURL(NSURL(string: "http://www.stackoverflow.com")!)
            }
        }
        tableView.reloadData()
    }


//    func update() {
//        //update your table data here
//        dispatch_async(dispatch_get_main_queue()) {
//            self.data.reloadData()
//        }
//    }
    
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
         (cell as! PickerTableCell).watchFrameChanges()
    }
    
    override func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        (cell as! PickerTableCell).ignoreFrameChanges()
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath == selectedIndexPath{
            return PickerTableCell.expandedHeight
        } else {
            return PickerTableCell.defaultHeight
        }
    }

}
