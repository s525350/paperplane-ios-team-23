//
//  PlanTableViewController.swift
//  TestApplication
//
//  Created by PRAHASITH AKKIRAJU on 9/11/16.
//  Copyright © 2016 Manthena,Suprem Raju. All rights reserved.
//

import UIKit

class PlanTableViewController: UITableViewController {
    
    var imageInfo: PlanTableViewCell!
    
    
    
    override func viewDidLoad() {
        imageInfo = PlanTableViewCell()
        super.viewDidLoad()
         self.title = "Day"

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismiss() {
        dismissViewControllerAnimated(true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PlanTableViewCell
        
        // Configure the cell...
        if indexPath.row == 0 {
            cell.postImageView.image = UIImage(named: "Chicago_willisTower")
            cell.postTitle.text = "Willis Tower"
            cell.postAuthor.text = "Day 1"
            cell.authorImageView.image = UIImage(named: "appcoda-300")
            
            
            
            
            
        } else if indexPath.row == 1 {
            cell.postImageView.image = UIImage(named: "Chicago_millenniumPark")
            cell.postTitle.text = "Millennium Park"
            cell.postAuthor.text = "Day 2"
            cell.authorImageView.image = UIImage(named: "appcoda-300")
            
        } else if indexPath.row == 2 {
            cell.postImageView.image = UIImage(named: "Chicago_360Tilt")
            cell.postTitle.text = "360 Tilt"
            cell.postAuthor.text = "Day 3"
            cell.authorImageView.image = UIImage(named: "appcoda-300")
            
        } else {
            cell.postImageView.image = UIImage(named: "Chicago_FieldMuseum")
            cell.postTitle.text = "Field Museum"
            cell.postAuthor.text = "Day 4"
            cell.authorImageView.image = UIImage(named: "appcoda-300")
            
        }
        
        return cell
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let destination = segue.destinationViewController as? DeyailViewController
        let row = tableView.indexPathForSelectedRow?.row
        destination?.imageData = imageInfo.logoImage[row!]
    }

    
    // MARK: - Navigation
    }
    
    

