//
//  HomeViewController.swift
//  Paperplane
//
//  Created by Shashikanth Reddy on 9/9/16.
//  Copyright © 2016 Abbu,Srilatha Reddy. All rights reserved.
//

import UIKit

class HomeViewController:UIViewController {
    var settingLauncher : SidebarMain!
    var setting : settingsData!
    @IBAction func listBtn(sender: AnyObject) {
      self.settingLauncher.showSide()
    }
    func assignbackground(){
        let background = UIImage(named: "landing_2")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.ScaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        assignbackground()
        settingLauncher = SidebarMain()
        
       //settingLauncher.scrennname = "Home"
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        setting = appDelegate.setting
        
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        self.setting.home = self
    }
}
