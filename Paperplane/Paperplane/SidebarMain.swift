//
//  SidebarMain.swift
//  SideBarAnimated
//
//  Created by PRAHASITH AKKIRAJU on 10/10/16.
//  Copyright © 2016 Prahasith Akkiraju. All rights reserved.
//

import Foundation
import UIKit

class Side : NSObject{
    let name : String
    init(name : String ) {
        self.name = name
    
}
}

class SidebarMain : NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    var setting : settingsData!
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var scrennname : String = "Home"
    let blackView = UIView()
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.whiteColor()
        return cv
    }()
    //Array of all the side bar elements
    let side : [Side]  = {
        return [Side(name: "Home"),Side(name: "Ratings"),Side(name: "Settings"),Side(name: "Feedback"),Side(name: "History"),Side(name: "Login")]
    }()
    
    let cellId = "cellId"
    func showSide() {
        if let window = UIApplication.sharedApplication().keyWindow {
            
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            window.addSubview(blackView)
            window.addSubview(collectionView)
            let x : CGFloat = 200
            let y = window.frame.width - x
            collectionView.frame = CGRect(x: 0, y: 0, width: 0, height: window.frame.height)
            blackView.frame = window.frame
            blackView.alpha = 0
            UIView.animateWithDuration(1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseOut, animations: {
                self.blackView.alpha = 1
                self.collectionView.frame = CGRect(x: 0, y: 0, width: 200, height: self.collectionView.frame.height)
                }, completion: nil)
        
        }
    }
    
    
    
    func handleDismiss(setting : Side){
        self.setting = appDelegate.setting
        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseOut, animations: {
            self.blackView.alpha = 0
            self.collectionView.frame = CGRect(x: 0, y: 0, width: 0, height: self.collectionView.frame.height)
        }) {(completed : Bool) in
            if setting.name != "" && setting.name != "Cancel" {
                if setting.name == "Home" {
                    self.setting.home.dismissViewControllerAnimated(true, completion: nil)
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("primary") as UIViewController
                    self.setting.home.presentViewController(vc, animated: true, completion: nil)
                } else if setting.name == "Settings" {
                    self.setting.home.dismissViewControllerAnimated(true, completion: nil)
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("setting") as UIViewController
                    self.setting.home.presentViewController(vc, animated: true, completion: nil)
                } else if setting.name == "Ratings"{
                    self.setting.home.dismissViewControllerAnimated(true, completion: nil)
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("rating") as UIViewController
                    self.setting.home.presentViewController(vc, animated: true, completion: nil)
                } else if setting.name == "Feedback"{
                    self.setting.home.dismissViewControllerAnimated(true, completion: nil)
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("feedback") as UIViewController
                    self.setting.home.presentViewController(vc, animated: true, completion: nil)
                } else if setting.name == "History"{
                    self.setting.home.dismissViewControllerAnimated(true, completion: nil)
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("history") as UIViewController
                    self.setting.home.presentViewController(vc, animated: true, completion: nil)
                } else {
                    self.setting.home.dismissViewControllerAnimated(true, completion: nil)
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("login") as UIViewController
                    self.setting.home.presentViewController(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return side.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellId, forIndexPath: indexPath) as! CollectionViewTable
        let setting = side[indexPath.item]
        cell.setting = setting
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 200, height: 50)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let select = side[indexPath.item]
        //print(select)
        self.handleDismiss(select)
    }
    
    func dismiss(){
        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseOut, animations: {
            self.blackView.alpha = 0
            self.collectionView.frame = CGRect(x: 0, y: 0, width: 0, height: self.collectionView.frame.height)
        },completion: nil)
    }
    
    
    override init() {
        super.init()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerClass(CollectionViewTable.self, forCellWithReuseIdentifier: cellId)
        
    }
    }

